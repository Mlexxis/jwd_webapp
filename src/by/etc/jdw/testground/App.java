package by.etc.jdw.testground;

import by.etc.jdw.testground.bean.Profile;
import by.etc.jdw.testground.bean.User;
import by.etc.jdw.testground.dao.db_simulation.ProfileTable;
import by.etc.jdw.testground.service.ServiceManager;
import by.etc.jdw.testground.service.service_interface.UserService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class App {

    private static final Logger log = LogManager.getRootLogger();

    public static void main(String[] args) {
        //log.info(PasswordEncryptor.encrypt("ssssssss"));
        //log.info(PasswordEncryptor.encrypt("ssssssss"));
        //log.info(UserValidator.isPasswordValid("SAF1dfds"));
        //log.info(UserValidator.isLoginValid("aaa@dsfsdf.by".toUpperCase()));

        User u1 = new User("a@a.by", "asddA1111");
        User u2 = new User("b@b.by", "2222");
        User u3 = new User("cc.by", "3333");
        User u4 = new User("d@d.by", "4444");

        Profile p1 = new Profile("Ivan", "Ivanov", "a@a.by", 20);
        Profile p2 = new Profile("John", "Smith", "b@b.by", 25);
        Profile p3 = new Profile("Tom", "Willice", "c@c.by", 31);
        Profile p4 = new Profile("Kate", "Petrova", "d@d.by", 19);


        UserService userManager = ServiceManager.getInstance().getUserHandler();


        System.out.println(userManager.register(u1, "asddA1111"));

        u1.setPassword("asddA1111");

        System.out.println(userManager.register(u1, "ssddA1111"));

        //userManager.submitProfile(u1, p1);
        //userManager.login(u1);



    }
}
