package by.etc.jdw.testground.bean;

import java.io.Serializable;

public class User implements Serializable {

    private static final long serialVersionUID = -3399017014371615280L;
    private String login;
    private String password;
    private UserRanks rank;

    public User() {
        rank = UserRanks.ANONYMOUS;
    }

    public User(String login, String password) {
        this();
        this.login = login.toUpperCase();
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login.toUpperCase();
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserRanks getRank() {
        return rank;
    }

    public void setRank(UserRanks rank) {
        this.rank = rank;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("User{");
        sb.append("login='").append(login).append('\'');
        sb.append(", password='").append(password).append('\'');
        sb.append(", rank=").append(rank);
        sb.append('}');
        return sb.toString();
    }
}
