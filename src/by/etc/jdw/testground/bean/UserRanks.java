package by.etc.jdw.testground.bean;

public enum UserRanks {
    ANONYMOUS, NOVICE, EXPERT, MASTER
}
