package by.etc.jdw.testground.dao.dao_interface;

import java.util.List;

public interface DataRepository<T> {
    void add (T item);
    void remove(T item);
    void update(T item);
    T find(String key);
    List<T> query (Specification spec);
}
