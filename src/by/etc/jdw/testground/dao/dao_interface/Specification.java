package by.etc.jdw.testground.dao.dao_interface;

public interface Specification {
    String toQueryString();
}
