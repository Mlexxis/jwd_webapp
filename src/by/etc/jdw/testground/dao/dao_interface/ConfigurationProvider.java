package by.etc.jdw.testground.dao.dao_interface;

import by.etc.jdw.testground.dao.exception.ConfigurationException;

public interface ConfigurationProvider<T> {

    T acquireConfiguration () throws ConfigurationException;

}
