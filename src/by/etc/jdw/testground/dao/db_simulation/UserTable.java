package by.etc.jdw.testground.dao.db_simulation;

import by.etc.jdw.testground.bean.User;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class UserTable {

    private List<User> users;

    public UserTable() {
        users = new ArrayList<>();
    }

    public void add(User user) {
        //
        System.out.println("Adding to table");

        User clone = new User();
        clone.setLogin(user.getLogin());
        clone.setPassword(user.getPassword());
        clone.setRank(user.getRank());
        users.add(clone);

        //
        System.out.println("Current User table:");
        showTable();
        System.out.println();
    }

    public void update(User user) {
        //
        System.out.println("Updating in table");

        users.removeIf(oldUser -> oldUser.getLogin().equals(user.getLogin()));
        User clone = new User();
        clone.setLogin(user.getLogin());
        clone.setPassword(user.getPassword());
        clone.setRank(user.getRank());
        users.add(clone);
        //
        System.out.println("Current User table:");
        showTable();
        System.out.println();

    }

    public void delete(User user) {
        //
        System.out.println("Deleting from table");


        users.removeIf(deleteUser -> deleteUser.getLogin().equals(user.getLogin()));
       //
        System.out.println("Current User table:");
        showTable();
        System.out.println();
    }

    public User find(String login) {
        for (User user : users) {
            if (user.getLogin().equals(login.toUpperCase())) {
                return user;
            }
        }
        return null;
    }

    public List<User> findBy(String spec) {
        String searchSpec = spec.substring(0, spec.indexOf('|'));
        String searchValue = spec.substring(spec.indexOf('|')+1);
        switch (searchSpec) {
            case "LOGIN": {
                return findUserByLogin(searchValue);
            }
            case "RANK": {
                return findUserByRank(searchValue);
            }
        }
        return Collections.emptyList();
    }

    private List<User> findUserByLogin(String login) {
        List<User> result = new ArrayList<>();
        for (User user : users) {
            if (user.getLogin().equals(login)) {
                result.add(user);
            }
        }
        return result;
    }

    private List<User> findUserByRank(String rank) {
        List<User> result = new ArrayList<>();
        for (User user : users) {
            if (user.getRank().name().equals(rank)) {
                result.add(user);
            }
        }
        return result;
    }

    //remove
    public void showTable() {
        for (User user : users) {
            System.out.println(user);
        }
    }
}
