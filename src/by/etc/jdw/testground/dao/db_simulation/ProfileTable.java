package by.etc.jdw.testground.dao.db_simulation;

import by.etc.jdw.testground.bean.Profile;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ProfileTable {

    private List<Profile> profiles;

    public ProfileTable() {
        profiles = new ArrayList<>();
    }

    public void add(Profile profile) {
        //
        System.out.println("Adding to table");

        Profile clone = new Profile();
        clone.setFirstName(profile.getFirstName());
        clone.setLastName(profile.getLastName());
        clone.setEmail(profile.getEmail());
        clone.setAge(profile.getAge());
        profiles.add(clone);

        //
        System.out.println("Current Profile table:");
        showTable();
        System.out.println();
    }

    public void update(Profile profile) {
        //
        System.out.println("Updating in table");

        profiles.removeIf(oldProfile -> oldProfile.getEmail().equals(profile.getEmail()));
        Profile clone = new Profile();
        clone.setFirstName(profile.getFirstName());
        clone.setLastName(profile.getLastName());
        clone.setEmail(profile.getEmail());
        clone.setAge(profile.getAge());
        profiles.add(clone);

        //
        System.out.println("Current Profile table:");
        showTable();
        System.out.println();
    }

    public void delete(Profile profile) {
        //
        System.out.println("Deleting from table");

        profiles.removeIf(deleteProfile -> deleteProfile.getEmail().equals(profile.getEmail()));

        //
        System.out.println("Current Profile table:");
        showTable();
        System.out.println();
    }

    public Profile find(String email) {
        for (Profile profile : profiles) {
            if (profile.getEmail().equals(email)) {
                return profile;
            }
        }
        return null;
    }

    public List<Profile> findBy(String spec) {
        String searchSpec = spec.substring(0, spec.indexOf('|'));
        String searchValue = spec.substring(spec.indexOf('|')+1);
        switch (searchSpec) {
            case "AGE" :  {
                return findProfileByAge(Integer.valueOf(searchValue));
            }
            case  "EMAIL": {
                return findProfileByEmail(searchValue);
            }
        }
        return Collections.emptyList();
    }

    private List<Profile> findProfileByEmail(String email) {
        List<Profile> result = new ArrayList<>();
        for (Profile profile : profiles) {
            if (profile.getEmail().equals(email)) {
                result.add(profile);
            }
        }
        return result;
    }

    private List<Profile> findProfileByAge(int age) {
        List<Profile> result = new ArrayList<>();
        for (Profile profile : profiles) {
            if (profile.getAge() == age) {
                result.add(profile);
            }
        }
        return result;
    }

    //remove
    public void showTable() {
        for (Profile profile : profiles) {
            System.out.println(profile);
        }
    }
}
