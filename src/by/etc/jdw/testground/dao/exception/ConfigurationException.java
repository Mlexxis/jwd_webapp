package by.etc.jdw.testground.dao.exception;

public class ConfigurationException extends Exception {

    public ConfigurationException() {
        super();
    }

    public ConfigurationException(String message) {
        super(message);
    }

    public ConfigurationException(String message, Exception ex) {
        super(message, ex);
    }

    public ConfigurationException(Exception ex) {
        super(ex);
    }
}
