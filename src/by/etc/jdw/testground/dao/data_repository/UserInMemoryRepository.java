package by.etc.jdw.testground.dao.data_repository;

import by.etc.jdw.testground.bean.User;
import by.etc.jdw.testground.dao.dao_interface.DataRepository;
import by.etc.jdw.testground.dao.dao_interface.Specification;
import by.etc.jdw.testground.dao.db_simulation.UserTable;

import java.util.List;

public class UserInMemoryRepository implements DataRepository<User> {

    private UserTable table;

    public UserInMemoryRepository() {
        table = new UserTable();
    }

    @Override
    public void add(User item) {
        table.add(item);
    }

    @Override
    public void remove(User item) {
        table.delete(item);
    }

    @Override
    public void update(User item) {
        table.update(item);
    }

    @Override
    public User find(String key) {
        return table.find(key);
    }

    @Override
    public List<User> query(Specification spec) {
        return table.findBy(spec.toQueryString());
    }
}
