package by.etc.jdw.testground.dao.data_repository;

import by.etc.jdw.testground.bean.Profile;
import by.etc.jdw.testground.dao.dao_interface.DataRepository;
import by.etc.jdw.testground.dao.dao_interface.Specification;
import by.etc.jdw.testground.dao.db_simulation.ProfileTable;

import java.util.List;

public class ProfileInMemoryRepository implements DataRepository<Profile> {

    private ProfileTable table;

    public ProfileInMemoryRepository() {
        table = new ProfileTable();
    }

    @Override
    public void add(Profile item) {
        table.add(item);
    }

    @Override
    public void remove(Profile item) {
        table.delete(item);
    }

    @Override
    public void update(Profile item) {
        table.update(item);
    }

    @Override
    public Profile find(String key) {
        return table.find(key);
    }

    @Override
    public List<Profile> query(Specification spec) {
        return table.findBy(spec.toQueryString());
    }
}
