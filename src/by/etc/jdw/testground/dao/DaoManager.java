package by.etc.jdw.testground.dao;


import by.etc.jdw.testground.bean.Profile;
import by.etc.jdw.testground.bean.User;
import by.etc.jdw.testground.dao.dao_interface.DataRepository;
import by.etc.jdw.testground.dao.data_repository.ProfileInMemoryRepository;
import by.etc.jdw.testground.dao.data_repository.UserInMemoryRepository;

public class DaoManager {

    private DaoManager() {}

    public static DaoManager getInstance() {
        return InstanceHolder.instance;
    }

    public DataRepository<User> getUserInMemoryRepositoryInstance() {
        return UserManagerInstanceHolder.instance;
    }

    public DataRepository<Profile> getProfileInMemoryRepositoryInstance() {
        return ProfileManagerInstanceHolder.instance;
    }

    private static class UserManagerInstanceHolder {
        private static final UserInMemoryRepository instance = new UserInMemoryRepository();
    }

    private static class ProfileManagerInstanceHolder {
        private static final ProfileInMemoryRepository instance = new ProfileInMemoryRepository();
    }

    private static class InstanceHolder {
        private static final DaoManager instance = new DaoManager();
    }


}
