package by.etc.jdw.testground.dao.sql;

import by.etc.jdw.testground.dao.sql.exception.ConnectionPoolException;
import by.etc.jdw.testground.dao.exception.ConfigurationException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.concurrent.LinkedBlockingQueue;

public class ConnectionPool {

    private static final Logger log = LogManager.getRootLogger();
    private static ConnectionConfiguration configuration;
    private LinkedBlockingQueue<Connection> pool;
    private int poolSize;

    private ConnectionPool() {

        poolSize = 10;
        pool = new LinkedBlockingQueue<>(poolSize);

    }

    public void init() throws ConnectionPoolException {
        try {
            configuration = new SqlDbConfigurationProvider().acquireConfiguration();

            String url = configuration.getDB_DRIVER() + configuration.getDB_HOST() + configuration.getDB_PORT();

            //!!!!!!
            for (int i = 0; i < poolSize; i++) {
                Connection connection = DriverManager.getConnection(url, configuration.getUSER(), configuration.getPASSWORD());
                pool.put(connection);
            }

        } catch (ConfigurationException ex) {
            log.error("Cannot read ConnectionPool configuration", ex);
            throw new ConnectionPoolException("Cannot read ConnectionPool configuration", ex);
        } catch (SQLException ex) {
            log.error("Cannot create a connection", ex);
            throw new ConnectionPoolException("Cannot create a connection", ex);
        } catch (InterruptedException ex) {
            log.error("Cannot add a connection to pool", ex);
            throw new ConnectionPoolException("Cannot add a connection to pool", ex);
        }
    }

    private static class InstanceHolder {
        private static final ConnectionPool INSTANCE = new ConnectionPool();
    }

    public static ConnectionPool getInstance () {
        return InstanceHolder.INSTANCE;
    }
}
