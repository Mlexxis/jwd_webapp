package by.etc.jdw.testground.dao.sql;

import by.etc.jdw.testground.dao.dao_interface.ConfigurationProvider;
import by.etc.jdw.testground.dao.exception.ConfigurationException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class SqlDbConfigurationProvider implements ConfigurationProvider<ConnectionConfiguration> {

    private final String DRIVER = "driver";
    private final String HOST = "host";
    private final String PORT = "port";
    private final String USER = "user";
    private final String PASSWORD = "password";

    private static final Logger log = LogManager.getRootLogger();

    @Override
    public ConnectionConfiguration acquireConfiguration() throws ConfigurationException {
        Properties dbProperties = new Properties();

        try {
            dbProperties.load(new FileInputStream("resources/db.properties"));
        } catch (IOException ex) {
            log.error("Database configuration file not found", ex);
            throw new ConfigurationException("Database configuration file not found", ex);
        }

        String driver = dbProperties.getProperty(DRIVER);
        String host = dbProperties.getProperty(HOST);
        String port = dbProperties.getProperty(PORT);
        String user = dbProperties.getProperty(USER);
        String password = dbProperties.getProperty(PASSWORD);
        return new ConnectionConfiguration(driver, host, port, user, password);
    }
}
