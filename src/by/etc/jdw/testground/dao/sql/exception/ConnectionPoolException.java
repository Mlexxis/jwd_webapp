package by.etc.jdw.testground.dao.sql.exception;

public class ConnectionPoolException extends Exception {

    public ConnectionPoolException() {
        super();
    }

    public ConnectionPoolException(String message) {
        super(message);
    }

    public ConnectionPoolException(String message, Exception ex) {
        super(message, ex);
    }

    public ConnectionPoolException(Exception ex) {
        super(ex);
    }
}
