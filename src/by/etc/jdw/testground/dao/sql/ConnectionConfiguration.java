package by.etc.jdw.testground.dao.sql;

public class ConnectionConfiguration {

    private final String DB_DRIVER;
    private final String DB_HOST;
    private final String DB_PORT;
    private final String USER;
    private final String PASSWORD;

    public ConnectionConfiguration(String DB_DRIVER, String DB_HOST, String DB_PORT, String USER, String PASSWORD) {
        this.DB_DRIVER = DB_DRIVER;
        this.DB_HOST = DB_HOST;
        this.DB_PORT = DB_PORT;
        this.USER = USER;
        this.PASSWORD = PASSWORD;
    }

    public String getDB_DRIVER() {
        return DB_DRIVER;
    }

    public String getDB_HOST() {
        return DB_HOST;
    }

    public String getDB_PORT() {
        return DB_PORT;
    }

    public String getUSER() {
        return USER;
    }

    public String getPASSWORD() {
        return PASSWORD;
    }
}
