package by.etc.jdw.testground.dao.specification.inmemory_storage;

import by.etc.jdw.testground.dao.dao_interface.Specification;

public class FindProfileByAge implements Specification {

    private int age;

    public FindProfileByAge(int age) {
        this.age = age;
    }

    @Override
    public String toQueryString() {
        return "AGE|" + age;
    }
}
