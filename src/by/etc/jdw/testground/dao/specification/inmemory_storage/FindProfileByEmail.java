package by.etc.jdw.testground.dao.specification.inmemory_storage;

import by.etc.jdw.testground.dao.dao_interface.Specification;

public class FindProfileByEmail implements Specification {

    private String email;

    public FindProfileByEmail(String email) {
        this.email = email;
    }

    @Override
    public String toQueryString() {
        return "EMAIL|"+email;
    }
}
