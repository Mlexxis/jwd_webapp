package by.etc.jdw.testground.dao.specification.inmemory_storage;


import by.etc.jdw.testground.bean.UserRanks;
import by.etc.jdw.testground.dao.dao_interface.Specification;

public class FindUserByRankSpec implements Specification {

    private UserRanks rank;

    public FindUserByRankSpec(UserRanks rank) {
        this.rank = rank;
    }

    @Override
    public String toQueryString() {
        return "RANK|" + rank.name();
    }
}
