package by.etc.jdw.testground.dao.specification.inmemory_storage;

import by.etc.jdw.testground.dao.dao_interface.Specification;

public class FindUserByLoginSpec implements Specification {

    private String login;

    public FindUserByLoginSpec(String login) {
        this.login = login;
    }

    @Override
    public String toQueryString() {
        return "LOGIN|" + login;
    }
}
