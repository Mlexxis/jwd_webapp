package by.etc.jdw.testground.controller;

import by.etc.jdw.testground.controller.controller_interface.RequestHandler;
import by.etc.jdw.testground.controller.request_enum.GetRequestUrls;
import by.etc.jdw.testground.controller.request_enum.PostRequestUrls;
import by.etc.jdw.testground.controller.request_handler.get_handler.*;
import by.etc.jdw.testground.controller.request_handler.post_handler.SubmitLoginHandler;
import by.etc.jdw.testground.controller.request_handler.post_handler.SubmitRegistrationHandler;

import java.util.HashMap;
import java.util.Map;

public class RequestManager {

    private Map<PostRequestUrls, RequestHandler> postUrls;
    private Map<GetRequestUrls, RequestHandler> getUrls;

    private RequestManager() {
        getUrls = new HashMap<>();
        getUrls.put(GetRequestUrls.INDEX, new BackToMainHandler());
        getUrls.put(GetRequestUrls.LOGIN, new LoginHandler());
        getUrls.put(GetRequestUrls.REGISTER, new RegisterHandler());
        getUrls.put(GetRequestUrls.VIEW_PROFILE, new ViewProfileHandler());
        getUrls.put(GetRequestUrls.LOGINSUMMARY, new LoginSummaryHandler());
        getUrls.put(GetRequestUrls.REGISTERSUMMARY, new RegisterSummaryHandler());

        postUrls = new HashMap<>();
        postUrls.put(PostRequestUrls.LOGIN, new SubmitLoginHandler());
        postUrls.put(PostRequestUrls.REGISTER, new SubmitRegistrationHandler());
    }

    public static RequestManager getInstance() {
        return InstanceHolder.instance;
    }

    public RequestHandler methodGetHandlerFor(String url) {
        return getUrls.get(GetRequestUrls.valueOf(url));
    }
    public RequestHandler methodPostHandlerFor(String url) {
        return postUrls.get(PostRequestUrls.valueOf(url));
    }

    private static class InstanceHolder {
        private static RequestManager instance = new RequestManager();
    }
}
