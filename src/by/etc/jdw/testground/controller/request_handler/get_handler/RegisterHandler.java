package by.etc.jdw.testground.controller.request_handler.get_handler;

import by.etc.jdw.testground.controller.controller_interface.RequestHandler;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class RegisterHandler implements RequestHandler {

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("WEB-INF/view/register/register.jsp").forward(request, response);
    }
}
