package by.etc.jdw.testground.controller.request_handler.post_handler;

import by.etc.jdw.testground.bean.Profile;
import by.etc.jdw.testground.bean.User;
import by.etc.jdw.testground.controller.controller_interface.RequestHandler;
import by.etc.jdw.testground.controller.request_enum.GetRequestUrls;
import by.etc.jdw.testground.service.ServiceManager;
import by.etc.jdw.testground.service.service_interface.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class SubmitLoginHandler implements RequestHandler {

    private UserService manager = ServiceManager.getInstance().getUserHandler();

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String login = request.getParameter("login");
        String password = request.getParameter("password");

        manager.login(new User(login, password));

        User loggedUser = manager.getRegisteredUser(login);

        HttpSession currentSession = request.getSession();
        currentSession.setAttribute("loggedUser", loggedUser);

        Profile currentProfile = manager.getUserProfile(login);

        if (currentProfile != null) {
            currentSession.setAttribute("currentProfile", currentProfile);
        }
        response.sendRedirect("loginSummary");
    }
}
