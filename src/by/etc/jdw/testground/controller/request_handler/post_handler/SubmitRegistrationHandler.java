package by.etc.jdw.testground.controller.request_handler.post_handler;

import by.etc.jdw.testground.bean.User;
import by.etc.jdw.testground.service.ServiceManager;
import by.etc.jdw.testground.service.service_interface.UserService;
import by.etc.jdw.testground.service.user_handler.UserHandleMistakes;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class SubmitRegistrationHandler implements by.etc.jdw.testground.controller.controller_interface.RequestHandler {

    private final UserService manager = ServiceManager.getInstance().getUserHandler();

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String login = request.getParameter("login");
        String password = request.getParameter("password");
        String retypePassword = request.getParameter("retypePassword");

        List<UserHandleMistakes> registrationMistakes = manager.register(new User(login, password), retypePassword);

        if(registrationMistakes.isEmpty()) {

            User registeredUser = manager.getRegisteredUser(login);

            request.getSession().setAttribute("registeredUser", registeredUser);
            response.sendRedirect("/registerSummary");

        } else {
            request.setAttribute("mistakes", registrationMistakes);
            request.getRequestDispatcher("WEB-INF/view/register/register.jsp").forward(request, response);
        }


    }
}
