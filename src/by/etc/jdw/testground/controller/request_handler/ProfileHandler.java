package by.etc.jdw.testground.controller.request_handler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ProfileHandler implements by.etc.jdw.testground.controller.controller_interface.RequestHandler {
    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/view/profile/profile.jsp").forward(request, response);
    }
}
