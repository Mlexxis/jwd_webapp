package by.etc.jdw.testground.controller.request_handler;

import by.etc.jdw.testground.bean.Profile;
import by.etc.jdw.testground.bean.User;
import by.etc.jdw.testground.service.ServiceManager;
import by.etc.jdw.testground.service.service_interface.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class SubmitProfileHandler implements by.etc.jdw.testground.controller.controller_interface.RequestHandler {

    private UserService manager = ServiceManager.getInstance().getUserHandler();

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        User currentUser = (User) request.getSession().getAttribute("loggedUser");

        String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");
        String email = currentUser.getLogin();
        int age = Integer.parseInt(request.getParameter("age"));

        manager.submitProfile(currentUser, new Profile(firstName, lastName, email, age));

        HttpSession session = request.getSession();
        session.setAttribute("loggedUserProfile", manager.getUserProfile(currentUser.getLogin()));

        request.getRequestDispatcher("/view/profile/profileSummary.jsp").forward(request, response);
    }
}
