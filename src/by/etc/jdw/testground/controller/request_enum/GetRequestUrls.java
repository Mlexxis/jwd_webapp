package by.etc.jdw.testground.controller.request_enum;

public enum GetRequestUrls {
    REGISTER, REGISTERSUMMARY, LOGIN, INDEX, VIEW_PROFILE, LOGINSUMMARY
}
