package by.etc.jdw.testground.controller.controller_interface;

public interface RequestErrorHandler {
    void handleError(String message);
}
