package by.etc.jdw.testground.controller.controller_interface;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public interface RequestHandler {
    void handle(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException;
}
