package by.etc.jdw.testground.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class FrontController extends HttpServlet {

    private static final long serialVersionUID = -907793340442865463L;
    private final int URL_PARSE_START_INDEX = 1;
    private static final Logger log = LogManager.getRootLogger();

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("Get: " + request.getServletPath());
        String url = request.getServletPath().substring(URL_PARSE_START_INDEX).toUpperCase();
        RequestManager.getInstance().methodGetHandlerFor(url).handle(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("Post: " + request.getServletPath());
        String url = request.getServletPath().substring(URL_PARSE_START_INDEX).toUpperCase();
        RequestManager.getInstance().methodPostHandlerFor(url).handle(request, response);
    }


}
