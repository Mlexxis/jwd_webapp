package by.etc.jdw.testground.service.user_handler;

public enum UserHandleMistakes {
    INVALID_LOGIN {
        public String message() {
          return "Invalid email. it should look like someone@someone.com";
        }
    }, INVALID_PASSWORD {
        public String message() {
            return "Invalid password. It must contain at least 1 capital letter, 1 regular letter, 1 digit, length is between 8 and 15 characters";
        }
    }
    , PASSWORD_MISMATCH {
        public String message() {
            return "Passwords you entered are different";
        }
    }, ALREADY_REGISTERED {
        public String message() {
            return "You are already registered";
        }
    };

    public abstract String message();

}
