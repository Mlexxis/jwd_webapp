package by.etc.jdw.testground.service.user_handler;

import by.etc.jdw.testground.bean.Profile;
import by.etc.jdw.testground.bean.User;
import by.etc.jdw.testground.bean.UserRanks;
import by.etc.jdw.testground.dao.DaoManager;
import by.etc.jdw.testground.dao.dao_interface.DataRepository;
import by.etc.jdw.testground.service.service_interface.UserService;
import by.etc.jdw.testground.service.utils.PasswordEncryptor;
import by.etc.jdw.testground.service.utils.UserValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.ArrayList;
import java.util.List;

public class UserHandler implements UserService {

    private static final Logger log = LogManager.getRootLogger();
    private final DataRepository<User> userDataRepository = DaoManager.getInstance().getUserInMemoryRepositoryInstance();
    private final DataRepository<Profile> profileDataRepository = DaoManager.getInstance().getProfileInMemoryRepositoryInstance();

    @Override
    public List<UserHandleMistakes> register(User user, String retypePassword) {
        log.info("Registering : " + user);

        List<UserHandleMistakes> mistakes = new ArrayList<>();

        if (hasValidCredentials(user, retypePassword, mistakes)) {
            if (!isRegistered(user)) {
                registerValidUser(user);
                //statusList.add(UserHandleMistakes.OK);
                log.info(user.getLogin() + " successfully registered");
            } else {
                mistakes.add(UserHandleMistakes.ALREADY_REGISTERED);
                log.info("You already registered");
            }
        }
        return mistakes;
    }

    @Override
    public void login(User user) {
        if(isRegistered(user)) {
            if (isCorrectPassword(user.getLogin(), user.getPassword())) {
                log.info("You successfully logged in");
                Profile currentUserProfile = profileDataRepository.find(user.getLogin());
                //log.info("Welcome, " + currentUserProfile.getFirstName() + " " + currentUserProfile.getLastName());
            } else log.info("Incorrect password");
        } else log.info("Yot are not registered yet");
    }

    @Override
    public User getRegisteredUser(String login) {
        return userDataRepository.find(login);
    }



    @Override
    public void submitProfile(User user, Profile profile) {
        log.info("Submitting profile for user " + user.getLogin());
        if (isRegistered(user)) {
            profileDataRepository.add(profile);
            log.info("Profile added");
        } else log.info("Yot are not registered yet");
    }

    @Override
    public Profile getUserProfile(String login) {
        return profileDataRepository.find(login);
    }

    private boolean isCorrectPassword(String login, String password) {
        log.info("Verifying password for user: " + login);
        log.info("current password is: " + password);
        User verifiedUser = userDataRepository.find(login);
        boolean result = PasswordEncryptor.encrypt(password).equals(verifiedUser.getPassword());
        String res = result ? "accepted" : "denied";
        log.info("Password " + res);
        return result;
    }

    private boolean hasValidCredentials(User user, String retypePassword, List<UserHandleMistakes> statusList) {
        boolean result = true;
        if (!retypePassword.equals(user.getPassword())) {
            result = false;
            statusList.add(UserHandleMistakes.PASSWORD_MISMATCH);
        }
        if (!UserValidator.isLoginValid(user.getLogin())) {
            result = false;
            statusList.add(UserHandleMistakes.INVALID_LOGIN);
        }
        if (!UserValidator.isPasswordValid(user.getPassword())) {
            result = false;
            statusList.add(UserHandleMistakes.INVALID_PASSWORD);
        }
        String res = result ? "OK" : "Invalid";
        log.info("Validating entered credentials quality: " + res);
        return result;
    }

    private boolean isRegistered(User user) {
        boolean result = userDataRepository.find(user.getLogin()) != null;
        String res = result ? "Yes" : "No";
        log.info("Checking if already registered: " + res);
        return result;
    }

    private void registerValidUser(User user) {
        user.setRank(UserRanks.NOVICE);
        user.setPassword(PasswordEncryptor.encrypt(user.getPassword()));
        log.info("Registering new valid user:" + user);
        userDataRepository.add(user);
    }

}
