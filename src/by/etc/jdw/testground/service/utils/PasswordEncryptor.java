package by.etc.jdw.testground.service.utils;

import org.apache.commons.codec.digest.DigestUtils;

public class PasswordEncryptor {

    public static String encrypt(String password) {
        return DigestUtils.md5Hex(password);
    }
}
