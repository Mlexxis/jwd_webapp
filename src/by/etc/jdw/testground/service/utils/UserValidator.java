package by.etc.jdw.testground.service.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserValidator {

    private static final String passwordRegex = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{8,15}";
    private static final String loginRegex = "[A-Z\\d._%+-]+@[A-Z\\d.-]+\\.[A-Z]{2,}";

    public static boolean isPasswordValid(String password) {
        Pattern pattern = Pattern.compile(passwordRegex);
        Matcher matcher = pattern.matcher(password);
        return matcher.matches();
    }

    public static boolean isLoginValid(String login) {
        Pattern pattern = Pattern.compile(loginRegex);
        Matcher matcher = pattern.matcher(login);
        return matcher.matches();
    }

}
