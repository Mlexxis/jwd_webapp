package by.etc.jdw.testground.service.service_interface;

import by.etc.jdw.testground.bean.Profile;
import by.etc.jdw.testground.bean.User;
import by.etc.jdw.testground.service.user_handler.UserHandleMistakes;

import java.util.List;

public interface UserService {

    List<UserHandleMistakes> register(User user, String retypePassword);
    User getRegisteredUser(String login);
    void login(User user);
    void submitProfile(User user, Profile profile);
    Profile getUserProfile(String login);
}
