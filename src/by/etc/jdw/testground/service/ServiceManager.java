package by.etc.jdw.testground.service;

import by.etc.jdw.testground.service.service_interface.UserService;
import by.etc.jdw.testground.service.user_handler.UserHandler;

public class ServiceManager {

    private ServiceManager() {
    }

    public static ServiceManager getInstance() {
        return InstanceHolder.instance;
    }

    public UserService getUserHandler() {
        return UserHandlerInstanceHolder.instance;
    }

    private static class InstanceHolder {
        private static final ServiceManager instance = new ServiceManager();
    }

    private static class UserHandlerInstanceHolder {
        private static final UserService instance = new UserHandler();
    }
}
