<%@ page import="by.etc.jdw.testground.bean.User" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>TestGround : Profile</title>
</head>
<body>
    <h1>PROFILE</h1>
    <p>You are logged as: ${loggedUser.getLogin()}</p>
    <form action="profile" method="post">
        <input type="hidden" name="command" value="SUBMIT_PROFILE">
        <label for="firstName">Your first name:</label>
        <input type="text" id="firstName" name="firstName">
        <label for="lastName">Your last name:</label>
        <input type="text" id="lastName" name="lastName">
        <label for="age">Your age:</label>
        <input type="text" id="age" name="age">
        <button type="submit">Add profile</button>
    </form>
    <form action="profile" method="get">
        <input type="hidden" name="command" value="LOGOUT">
        <button type="submit">Log Out</button>
    </form>
</body>
</html>
