<style type="text/css">
    .header {
        padding: 5px;
        display: block;
        min-width: 700px;
        max-width: 1680px;
    }
    .header_links {
        padding: 5px;
        display: inline-block;
        color: grey;
        width: 10%;
        text-align: center;
        background-color: darkslategrey;
    }
    .header_links a {
        color: white;
    }
    .header_rest { padding: 5px;
        display: inline-block;
        color: darkslategrey;
        text-align: center;
        background-color: darkslategrey;
        width: 60%;
    }
</style>
<header>
    <div class="header">
        <div class="header_links">
            <a href="index">Main Page</a>
        </div>
        <div class="header_links">
            <a href="login">Log in</a>
        </div>
        <div class="header_links">
            <a href="register">Register</a>
        </div>
        <div class="header_rest">a</div>
    </div>
</header>