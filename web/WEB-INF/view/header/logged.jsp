<style type="text/css">
    .header {
        padding: 5px;
        display: block;
        min-width: 700px;
        max-width: 1680px;
    }
    .header_links {
        padding: 5px;
        display: inline-block;
        color: grey;
        width: 10%;
        text-align: center;
        background-color: darkslategrey;
    }
    .header_links a {
        color: white;
    }
    .header_rest { padding: 5px;
        display: inline-block;
        color: darkslategrey;
        text-align: center;
        background-color: darkslategrey;
        width: 50%;
    }
</style>
<header>
    <div class="header">
        <div class="header_links">
            <a href="index">Main Page</a>
        </div>
        <div class="header_links">
            <a href="logout">Log out</a>
        </div>
        <div class="header_links">
            <a href="view_profile">Profile</a>
        </div>
        <div class="header_links">
            <a href="test">Take a test</a>
        </div>
        <div class="header_rest">a</div>
    </div>
</header>