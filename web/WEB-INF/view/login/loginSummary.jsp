<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ include file="../header/logged.jsp"%>
<html>
<head>
    <title>TestGround : LoginSummary</title>
    <link rel="stylesheet" href="../../../styles.css">
</head>
<body>

    <h3>You are logged as:  ${loggedUser.getLogin()}</h3>
    <h3>Your profile:  ${currentProfile}</h3>
    <form action="index" method="post">
        <input type="hidden" name="command" value="BACK_TO_INDEX">
        <button type="submit">Back to index</button>
    </form>
    <form action="profile" method="post">
        <input type="hidden" name="command" value="PROFILE">
        <button type="submit">Create profile</button>
    </form>
</body>
</html>
