<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ include file="../header/unregistered.jsp"%>
<html>
<head>
    <title>Test Ground: Login</title>
    <link rel="stylesheet" href="../../../styles.css">
</head>
<body>
<form class="container" action="login" method="post">
    <h3>Log In</h3>
    <div class="form_element">
        <label for="login">Your email:</label>
        <input id="login" type="text" name= "login" placeholder="someone@someone.com" required> <!--pattern="[a-zA-Z\d._%+-]+@[a-zA-Z\d.-]+\.[a-zA-Z]{2,}" -->
    </div>
    <div class="form_element">
        <label for="password">Your password:</label>
        <input id="password" type="text" name= "password" placeholder="some strong password" required ><!--pattern="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,15}" -->
    </div>
    <div class="form_element">
        <button type="submit">Log In</button>
    </div>
    <div class="mistake_message">
        <c:forEach var = "mistake" items="${mistakes}">
            <p>* ${mistake.message()}</p>
        </c:forEach>
    </div>
</form>

</body>
</html>
