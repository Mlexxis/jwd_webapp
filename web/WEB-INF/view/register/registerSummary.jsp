<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@include file="../header/unregistered.jsp" %>
<html>
<head>
    <title>TestGround : RegistrationSummary</title>
    <link rel="stylesheet" href="../../../styles.css">
</head>
<body>
    <div class="container">
        <h3>Registration</h3>
            <div class="form_element">
                <h3>You successfully registered at TestGround</h3>
                <p class="form_result">Your login: ${registeredUser.getLogin()} </p>
                <p class="form_result">Your rank changed to:  <b>${registeredUser.getRank()}</b> </p>
            </div>
            <div class="result_links">
                <a href="login">Log In</a>
                <a href="index">Main Page</a>
            </div>
    </div>
</body>
</html>
